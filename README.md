# Work sample

## Purpose
I am building a fairly generic front-end only app to demonstrate my proficiency in React/Redux/Bootstrap. I have a number of React apps for which I was the lead/sole developer, but nothing that is available or can be made available publicly.

## Getting started
* Clone the project using `git clone https://gitlab.com/joshgoss/react-work-sample.git`
* Install the dependencies using the package manager of your choice.  I use yarn, so I would do `yarn install`
* Start the server using the command `yarn start`
* View the app in your browser at `https://localhost:3000`
