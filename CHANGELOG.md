# Unreleased
## Added
* Initialized project with create react app typescript template ([#1](https://gitlab.com/joshgoss/react-work-sample/-/issues/1)).
* Added dev dependencies for testing and linting ([#2](https://gitlab.com/joshgoss/react-work-sample/-/issues/2)).
* Added react router ([#3](https://gitlab.com/joshgoss/react-work-sample/-/issues/3)).
* Added bootstrap and styled components ([#4](https://gitlab.com/joshgoss/react-work-sample/-/issues/4)).
* Added redux ([#5](https://gitlab.com/joshgoss/react-work-sample/-/issues/5)).
* Added navbar ([#6](https://gitlab.com/joshgoss/react-work-sample/-/issues/6)).
* Added query for weather by station ([#7](https://gitlab.com/joshgoss/react-work-sample/-/issues/7)).
* Added followed airport slice ([#8](https://gitlab.com/joshgoss/react-work-sample/-/issues/8)).
* Added airport follow form ([#9](https://gitlab.com/joshgoss/react-work-sample/-/issues/9)).
* Added airport index page ([#10](https://gitlab.com/joshgoss/react-work-sample/-/issues/10)).
