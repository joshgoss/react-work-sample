interface IStatus {
  value: number
  unitCode: string
}

interface IWeather {
  intensity: string | null
  modifier: string | null
  weather: string
}

interface IWeather {
  properties: {
    icon: string
    temperature: IStatus
    relativeHumidity: IStatus
    textDescription: string
  }
}

export default IWeather
