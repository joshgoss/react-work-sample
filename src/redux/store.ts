import { configureStore } from '@reduxjs/toolkit'

import { weatherApi } from './api/weather_api'
import followedAirportReducer from './slices/followedAirportSlice'

export default configureStore({
  reducer: {
    follow: followedAirportReducer,
    [weatherApi.reducerPath]: weatherApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(weatherApi.middleware),
})
