import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

import IWeather from '../../types/IWeather'

export const weatherApi = createApi({
  reducerPath: 'weatherApi',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_WEATHER_BASE_URL,
  }),
  tagTypes: ['Weather'],
  endpoints: (builder) => ({
    getAirportWeather: builder.query<IWeather, string>({
      query: (id) => ({
        url: `/stations/K${id.toUpperCase()}/observations/latest`,
      }),
      providesTags: ['Weather'],
    }),
    getAirportValidity: builder.query<'', string>({
      query: (id) => ({
        url: `/stations/K${id}`,
      }),
    }),
  }),
})

export const { useGetAirportWeatherQuery, useGetAirportValidityQuery } =
  weatherApi
