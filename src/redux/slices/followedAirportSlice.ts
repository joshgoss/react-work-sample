/* eslint-disable no-param-reassign */
import { createSlice } from '@reduxjs/toolkit'

import IAirports from '../../types/IAirports'

const initialState: IAirports = { value: ['LAX'] }

const followedAirportSlice = createSlice({
  name: 'follow',
  initialState,
  reducers: {
    follow: (state, action) => {
      state.value.push(action.payload)
    },
  },
})

export const { follow } = followedAirportSlice.actions
export default followedAirportSlice.reducer
