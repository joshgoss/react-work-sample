import styled from 'styled-components'

const AppLink = styled.a`
  color: #61dafb;
`

export default AppLink
