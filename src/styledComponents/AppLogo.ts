import styled, { keyframes } from 'styled-components'

const logoAnimation = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`

const AppLogo = styled.img`
  height: 40vmin;
  pointer-events: none;
  animation-name: ${logoAnimation};
  animation-duration: 6s;
  animation-iteration-count: infinite;
  animation-timing-function: linear;
`

export default AppLogo
