import React from 'react'

import Navbar from '../components/Navbar'
import AirportIndex from '../components/AirportIndex'

const AddAirport: React.FC = () => (
  <React.Fragment>
    <Navbar />
    <AirportIndex />
  </React.Fragment>
)

export default AddAirport
