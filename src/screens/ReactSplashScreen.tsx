import React from 'react'

import logo from '../assets/logo.svg'
import Application from '../styledComponents/App'
import AppHeader from '../styledComponents/AppHeader'
import AppLogo from '../styledComponents/AppLogo'
import AppLink from '../styledComponents/AppLink'
import Navbar from '../components/Navbar'

const App: React.FC = () => (
  <React.Fragment>
    <Navbar />
    <Application>
      <AppHeader>
        <AppLogo src={logo} alt="logo" />
        <div className="card bg-dark">
          <p>
            Edit <code>src/App.tsx</code> and save to reload.
          </p>
          <AppLink
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </AppLink>
        </div>
      </AppHeader>
    </Application>
  </React.Fragment>
)

export default App
