import React from 'react'

import HomeScreenContent from '../components/content/HomeScreenContent'
import Navbar from '../components/Navbar'

const App: React.FC = () => (
  <React.Fragment>
    <Navbar />
    <HomeScreenContent />
  </React.Fragment>
)

export default App
