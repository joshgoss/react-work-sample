import React from 'react'

import Navbar from '../components/Navbar'
import AddAirportForm from '../components/AddAirportForm'

const AddAirport: React.FC = () => (
  <React.Fragment>
    <Navbar />
    <AddAirportForm />
  </React.Fragment>
)

export default AddAirport
