import Enzyme from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import '@testing-library/jest-dom'
import { configureToMatchImageSnapshot } from 'jest-image-snapshot'

Enzyme.configure({ adapter: new Adapter() })
jest.setTimeout(60000)

const toMatchImageSnapshot = configureToMatchImageSnapshot({
  failureThreshold: 0.02,
  failureThresholdType: 'percent',
})
expect.extend({ toMatchImageSnapshot })
