/* eslint-disable react/no-array-index-key */
import React from 'react'
import { useSelector } from 'react-redux'

import IAirports from '../types/IAirports'

import AirportEntry from './AirportEntry'

const AirportIndex: React.FC = () => {
  const airports = useSelector(
    (state: { follow: IAirports }) => state.follow.value,
  )

  return (
    <div className="container">
      <div className="row row-cols-lg-3 row-cols-md-2 row-cols-1">
        {airports.map((airport, index) => (
          <div key={`airport${index}`} className="col">
            <AirportEntry code={airport} />
          </div>
        ))}
      </div>
    </div>
  )
}

export default AirportIndex
