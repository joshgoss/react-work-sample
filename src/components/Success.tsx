import React from 'react'

// I would typically make this just a flash message and pass in the type of message and
// the message itself, but this is the only one I need
const Success: React.FC = () => (
  <div className="alert alert-success" role="alert">
    The airport code has been added to your list
  </div>
)

export default Success
