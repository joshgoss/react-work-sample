import React, { useState } from 'react'
import { useDispatch } from 'react-redux'

import { follow } from '../redux/slices/followedAirportSlice'

import Success from './Success'
import TextFormField from './TextFormField'

const AddAirportForm: React.FC = () => {
  const [successShow, setSuccessShow] = useState<boolean>(false)
  const [airportCode, setAirportCode] = useState<string>('')
  const dispatch = useDispatch()

  const handleInputChange = (
    event: React.FormEvent<HTMLInputElement>,
  ): void => {
    const value = event.currentTarget.value || ''
    setAirportCode(value)
    setSuccessShow(false)
  }

  const submitHandler = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault()

    dispatch(follow(airportCode.toUpperCase()))
    setAirportCode('')
    setSuccessShow(true)
  }

  return (
    <div className="container">
      {successShow && <Success />}
      <h1>Add an airport</h1>
      <form onSubmit={submitHandler}>
        <TextFormField
          label="Airport's 3-letter code"
          groupName="newAirport"
          changeHandler={handleInputChange}
          required
          validityPattern="^[a-zA-Z]{3}$"
          value={airportCode}
        />
        <br />
        <button className="btn-primary" type="submit">
          Submit
        </button>
      </form>
    </div>
  )
}

export default AddAirportForm
