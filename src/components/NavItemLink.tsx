import React from 'react'
import { Link } from 'react-router-dom'

interface INavItemLinkProps {
  label: string
  href: string
}

const NavItemLink: React.FC<INavItemLinkProps> = ({ label, href }) => (
  <li className="nav-item">
    <Link className="nav-link" to={href}>
      {label}
    </Link>
  </li>
)

export default NavItemLink
