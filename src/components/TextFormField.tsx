/* eslint-disable jsx-a11y/label-has-for */
import React from 'react'

interface ITextFormField {
  label: string
  groupName: string
  changeHandler: React.ChangeEventHandler<HTMLInputElement>
  required?: boolean
  validityPattern?: string
  type?: string
  value: string
}

const TextFormField: React.FC<ITextFormField> = ({
  label,
  groupName,
  changeHandler,
  required = false,
  validityPattern = '.*',
  type = 'text',
  value,
}) => (
  <div>
    <label htmlFor={groupName} className="form-label col">
      {label}
    </label>
    <div>
      <input
        name={groupName}
        type={type}
        className="form-control"
        id={groupName}
        onChange={changeHandler}
        required={required}
        pattern={validityPattern}
        value={value}
      />
    </div>
  </div>
)

export default TextFormField
