import React from 'react'
import { Route, Switch } from 'react-router-dom'

import ReactSplash from '../screens/ReactSplashScreen'
import Home from '../screens/Home'
import AddAirport from '../screens/AddAirport'
import Airports from '../screens/Airports'

const Routes: React.FC = () => (
  <Switch>
    <Route path="/add_airport" component={AddAirport} />
    <Route path="/airports" component={Airports} />
    <Route path="/react_splash" component={ReactSplash} />
    <Route path="/" exact component={Home} />
  </Switch>
)

export default Routes
