import React from 'react'

import NavItemLink from './NavItemLink'

const Navbar: React.FC = () => (
  <nav className="navbar navbar-expand-lg bg-light mb-3">
    <div className="container">
      <div className="navbar-brand">Goss React Sample</div>
      <button
        className="navbar-toggler navbar-light"
        type="button"
        data-bs-toggle="collapse"
        data-bs-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <NavItemLink label="Home" href="/" />
          <NavItemLink
            label="Styled Component Splash Screen"
            href="/react_splash"
          />
          <NavItemLink label="Add an Airport" href="/add_airport" />
          <NavItemLink label="Airport information" href="/airports" />
        </ul>
      </div>
    </div>
  </nav>
)

export default Navbar
