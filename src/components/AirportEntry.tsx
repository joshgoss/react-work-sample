import React, { useEffect, useState } from 'react'

import { useGetAirportWeatherQuery } from '../redux/api/weather_api'

interface IAirportEntry {
  code: string
}

const AirportEntry: React.FC<IAirportEntry> = ({ code }) => {
  const [status, setStatus] = useState('loading')
  const { data, isSuccess, isError } = useGetAirportWeatherQuery(code)

  useEffect(() => {
    if (isError) {
      setStatus('error')
    } else if (isSuccess) {
      setStatus('ready')
    }
  }, [isError, isSuccess, data])

  return (
    <div className="border mb-3">
      <div className="text-center">{code}</div>
      {status === 'loading' && 'loading...'}
      {status === 'error' && (
        <div className="bg-danger text-center text-white">
          Could not find an airport for this value
        </div>
      )}
      {status === 'ready' && (
        <div>
          <div className="row">
            <div className="col">
              <img
                src={data?.properties.icon}
                alt={`${data?.properties.textDescription}`}
                className="img-thumbnail"
              />
            </div>
            <div className="col">
              {data?.properties.textDescription} <br />
              {data?.properties.temperature.value}&deg; C <br />
              {(data?.properties.temperature.value || 0 * 9) / 5 + 32}&deg; F
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

export default AirportEntry
