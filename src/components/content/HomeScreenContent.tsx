import React from 'react'

const HomeScreenContent: React.FC = () => (
  <div className="container">
    <h1>Welcome!</h1>
    <p>
      This is a very small react app built to demonstrate some fundamental
      knowledge.
    </p>
    <h2>Goal:</h2>
    <p>The goal is to build this in half a day, and incorporate</p>
    <ul>
      <li>
        Styled components: convert the react splash screen from css to styled
        components
      </li>
      <li>
        Bootstrap: set up the site to use bootstrap for styling and
        adaptability, including navbar
      </li>
      <li>Redux Toolkit: set up an API call and caching from a weather API</li>
      <li>
        Session management: use RTK Query polling to simulate refreshing a JWT
        to local storage
      </li>
      <li>
        Controlled components/state lifting: make a moderately complex form
      </li>
      <li>
        Test driven development: have all elements with basic snapshot testing -
        usually I would do a bit more, but this is more POC style.
      </li>
      <li>Code quality: using the standard linters to enfore code quality</li>
    </ul>
    <h2>Outcome</h2>
    <p>
      I had originally thought of half a day as half a work day - so four hours.
      I missed that mark by about an hour.
    </p>
    <p>
      I also did not have time to get the session management done, which is not
      particularly suprising. This is only my second time using Redux Toolkit
      and I hit a few snags.
    </p>
    <p>
      The elaborate form, I basically ran out of time and ideas. I have an
      example that I am more than happy to show, but it just wasn&apos;t in the
      cards for today. Regardless, there is a small form and the kind of
      rudiments are there.
    </p>
    <p>
      I should have stuck with the testing better. I made a couple of mistakes
      that I had to chase down that I would have found quickly, diagnosed
      immediately, and fixed easily, if I had just taken the time to test
      properly.
    </p>
    <p>
      If I had about 15 more minutes I would have thrown a delete button into
      the airport index screen, but again - trying to stick to the parameters I
      set for myself.
    </p>
    <h2>Conclusion</h2>
    <p>
      Overall, I&apos;m happy with it. It was an ambitious goal, and the main
      point was to get some quick practice in with redux toolkit while also
      getting a react work sample up on my public gitlab. Not bad for the time I
      could devote to it.
    </p>
  </div>
)

export default HomeScreenContent
