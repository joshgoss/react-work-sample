import { shallow } from 'enzyme'
import React from 'react'

import App from '../../../components/App'

describe('App', () => {
  it('renders', () => {
    const wrapper = shallow(<App />)

    expect(wrapper).toMatchSnapshot()
  })
})
