import { shallow } from 'enzyme'
import React from 'react'

import Navbar from '../../../components/Navbar'

describe('Navbar', () => {
  it('renders', () => {
    const wrapper = shallow(<Navbar />)

    expect(wrapper).toMatchSnapshot()
  })
})
