import { shallow } from 'enzyme'
import React from 'react'

import NavItemLink from '../../../components/NavItemLink'

describe('NavItemLink', () => {
  it('renders', () => {
    const wrapper = shallow(
      <NavItemLink label="link text" href="https://example.com" />,
    )

    expect(wrapper).toMatchSnapshot()
  })
})
