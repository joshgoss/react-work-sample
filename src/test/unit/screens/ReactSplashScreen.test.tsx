import React from 'react'
import { render, screen } from '@testing-library/react'
import { shallow } from 'enzyme'

import SplashScreen from '../../../screens/ReactSplashScreen'

describe('Splash screen', () => {
  it('renders learn react link', () => {
    render(<SplashScreen />)
    const linkElement = screen.getByText(/learn react/i)
    expect(linkElement).toBeInTheDocument()
  })

  it('matches the snapshot', () => {
    const wrapper = shallow(<SplashScreen />)

    expect(wrapper).toMatchSnapshot()
  })
})
