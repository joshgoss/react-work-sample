const fs = require('fs')

const prettierOptions = JSON.parse(fs.readFileSync('./.prettierrc', 'utf8'))

module.exports =  {
  parser:  '@typescript-eslint/parser',  // Specifies the ESLint parser
  extends:  [
    'airbnb',
    'eslint:recommended',
    "plugin:@typescript-eslint/eslint-recommended",
    'plugin:@typescript-eslint/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:jsx-a11y/strict',
    'plugin:react/recommended',
    'prettier',
    'react-app',
  ],
  plugins: [
    '@typescript-eslint',
    'import',
    'jsx-a11y',
    'prettier',
    'react',
  ],
  parserOptions:  {
    ecmaVersion:  2018,
    sourceType:  'module',
    ecmaFeatures:  {
      jsx:  true, 
    },
  },
  rules: {
    '@typescript-eslint/consistent-type-assertions': [
      2
    ],
    "@typescript-eslint/camelcase": 0,
    '@typescript-eslint/explicit-function-return-type': [
      'error',
      {
        allowExpressions: true
      }
    ],
    "@typescript-eslint/naming-convention": [
      "error",
      {
        "selector": "interface",
        "format": ["PascalCase"],
        "custom": {
          "regex": "^I[A-Z]",
          "match": true
        }
      }
    ],
    '@typescript-eslint/member-delimiter-style': [
      2,
      {
        'multiline': {
          'delimiter': 'none',
        }
      }
    ],
    '@typescript-eslint/no-explicit-any': 'error',
    '@typescript-eslint/no-unused-vars': 'error',
    'no-use-before-define': [0],
    '@typescript-eslint/no-use-before-define': [1],
    "import/extensions": [
      "error", "always",
      {
        "js": "never",
        "jsx": "never",
        "ts": "never",
        "tsx": "never"
      }
    ],
    'import/namespace': [
      2, 
      { 
        allowComputed: true 
      }
    ],
    "import/no-extraneous-dependencies": [
      "error", 
      {
        "devDependencies": true
      }
    ],
    'import/order': [
      'error',
      {
        groups: [
          'builtin',
          'external',
          'internal',
          'parent',
          'sibling',
          'index',
        ],
        'newlines-between': 'always',
      },
    ],
    'no-empty': [
      "error", 
      { 
        "allowEmptyCatch": true 
      }
    ],
    'no-prototype-builtins': 0,
    'prettier/prettier': [
      'error', 
      prettierOptions
    ],
    'react/forbid-prop-types': 0,
    'react/jsx-filename-extension': [
      1,
      { 
        extensions: [
          '.tsx', 
          '.ts'
        ] 
      },
    ],
    "react/jsx-fragments": [
      2,
      "element"
    ],
    'no-console': 2,
    'react/jsx-one-expression-per-line': 0,
    'react/prop-types': 0,
  },
  settings:  {
    react:  {
      version:  'detect',
    },
    "import/resolver": {
      "node": {
        "extensions": [".js", ".jsx", ".ts", ".tsx"]
      }
    }
  },
};
